* **EaaSI Build:** [Your info here! This corresponds to the eaasi-installer/release branch used to deploy your instance]
* **Browser:**  [Your info here! Please include version]
* **EaaSI Network Node (if applicable):** [Your info here!]


## Description of Bug

Please describe your issue in detail (including the name/ID of any relevant
environments, software, or content). You can also add screenshots or relevant
server log files using the "Attach a file" button!

## Reproduction

Is this bug reproducible? Please indicate the corresponding option:  
- Always
- Randomly
- Happened only once  


Steps to reproduce (if applicable):  
1.  
2.  
3.  
....

Expected result:

Actual result:  
  
  
  
/label ~"bug report"