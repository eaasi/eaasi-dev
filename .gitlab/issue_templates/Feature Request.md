* **EaaSI Build:** [Your info here!]
* **UI Build:** [Your info here!]
* **Browser** [Your info here!]
* **EaaSI Network Node (if applicable):** [Your info here!]


## Feature Request

Is there a feature or enhancement that you would like to see included in the EaaSI platform? Let us know a bit about what you'd like to do (concrete examples or use cases help!) and why the current platform doesn't address it.

If you've got any ideas for solutions (*how* we could solve or help your example), let us know that as well.

EaaSI staff will consider whether your proposal is in-scope given planned development, and let you know if your idea is included in the EaaSI roadmap! You might also consider posting to the [EaaSI Tech Talk](https://groups.google.com/forum/#!forum/eaasi-tech-talk) list to gauge interest and support within the EaaSI Network.



/label ~"IMPROVEMENT"
